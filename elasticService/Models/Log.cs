using System;

namespace elasticService.Model
{
    public class Log
    {
        public int UserID {get; set;}
        public DateTime PostDate {get;set;}
        public string Message{get;set;}
    }
}
