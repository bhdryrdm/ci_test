
using System;
using Nest;

namespace elasticService.Helper
{
    public static class Helper
    {
        private static readonly ConnectionSettings connectionSettings = new ConnectionSettings(new Uri("http://localhost:9200"))
        .DefaultIndex("log_history");

        public static readonly ElasticClient elasticClient = new ElasticClient(connectionSettings);
    }
}
