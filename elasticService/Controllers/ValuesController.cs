﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using elasticService.Model;
using Microsoft.AspNetCore.Mvc;
using Nest;

namespace elasticService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        ElasticClient elasticClient = Helper.Helper.elasticClient;
        [HttpPost("api/logs")]
        public void InsertLog([FromBody]Log log)
        {
            //elasticClient.DeleteIndex("log_history");

            //Yok ise error_log Index Oluşturma. Sadece 1 kez çalışır.
            if (!elasticClient.IndexExists("error_log").Exists)
            {
                var indexSettings = new IndexSettings();
                indexSettings.NumberOfReplicas = 1;
                indexSettings.NumberOfShards = 3;


                var createIndexDescriptor = new CreateIndexDescriptor("log_history")
               .Mappings(ms => ms.Map<Log>(m => m.AutoMap()))
                .InitializeUsing(new IndexState() { Settings = indexSettings })
                .Aliases(a => a.Alias("error_log"));

                var response = elasticClient.CreateIndex(createIndexDescriptor);
            }
            //Insert Data                                              
            elasticClient.Index<Log>(log, idx => idx.Index("log_history"));
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
