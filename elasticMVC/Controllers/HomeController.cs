﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using elasticMVC.Models;
using System.Net.Http;
using Newtonsoft.Json;
using elasticService.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace elasticMVC.Controllers
{
    public class HomeController : Controller
    {
        ILogger<HomeController> _logger;
        IConfiguration _configuration;
        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            _logger.LogError($"Bahadır Yardım: {DateTime.UtcNow} Index Page");
            ViewData["env_name"] = _configuration["environment"];
            return View();
        }


        public IActionResult About()
        {
            _logger.LogError($"Bahadır Yardım: {DateTime.UtcNow} About Page");
            return View();
        }

        public IActionResult Contact()
        {
            _logger.LogError($"Bahadır Yardım: {DateTime.UtcNow} Contact Page");
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
