FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY . .
WORKDIR /src/elasticMVC
RUN dotnet publish elasticMVC.csproj -c Release -o /app

FROM microsoft/dotnet:2.1-aspnetcore-runtime
WORKDIR /app
COPY --from=build /app .
RUN find . -name "*.pdb" -type f -delete
EXPOSE 80
ENTRYPOINT ["dotnet", "elasticMVC.dll"]